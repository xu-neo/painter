package neo.pro.paint;

import neo.pro.paint.server.MapServer;
import neo.pro.paint.ws.NioWebsocketServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class PaintApplication {
    public static void main(String[] args) {
        ApplicationContext applicationContext = SpringApplication.run(PaintApplication.class, args);
        NioWebsocketServer wsServer = applicationContext.getBean(NioWebsocketServer.class);
        MapServer mapServer = applicationContext.getBean(MapServer.class);
        mapServer.start();
        wsServer.run();
    }
}
