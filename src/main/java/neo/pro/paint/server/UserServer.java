package neo.pro.paint.server;

import io.netty.channel.Channel;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class UserServer {
    private ChannelGroup allChannel = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private Map<String, User> allUser = new HashMap<>();
    private int painterCount = 0;
    private int audienceCount = 0;

    public int painterSize(){
        return painterCount;
    }

    public int audienceSize(){
        return audienceCount;
    }

    public void addPainter(User user, Channel channel){
        allChannel.add(channel);
        allUser.put(user.getId(), user);
        painterCount ++;
    }

    public void addAudience(User user, Channel channel){
        allChannel.add(channel);
        allUser.put(user.getId(), user);
        audienceCount ++;
    }

    public User getUserByID(String id){
        return allUser.get(id);
    }

    public void validateUser(User user) throws BaseException {
        if (user.getName() != null && user.getName().length() > 50){
            throw new BaseException("user name is too long");
        }
    }

    public ChannelGroup getAllChannel() {
        return allChannel;
    }

    public Map<String, User> getAllUser() {
        return allUser;
    }

    public void userDisconnect(){

    }
}
