package neo.pro.paint.server;

import com.alibaba.fastjson.JSON;
import io.netty.channel.Channel;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.*;
import neo.pro.paint.utils.CircleList;
import neo.pro.paint.utils.CommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.List;

@Service
public class MapServer extends Thread{
    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Value("${paint.image.init-path}")
    private String imageName;

    @Value("${paint.image.snapshot.path}")
    private String snapshotPath;

    @Value("${paint.image.snapshot.count:4}")
    private int snapshotCount;

    @Value("${paint.image.snapshot.condition.commands:3000}")
    private int ssCmds;

    @Value("${paint.image.snapshot.condition.pixel:0.1}")
    private Float ssPixels;

    @Autowired
    private UserServer userServer;

    private int writedPixel = 0;

    private Canvas map;

    private int writePos = 0;

    private BufferedOutputStream writer;

    private CircleList<Command> commands;

    private CircleList<Snapshot> sss;

    private long lastFlush = System.currentTimeMillis();

    @PostConstruct
    public void init() throws IOException {
        commands = new CircleList<>(snapshotCount * ssCmds);
        sss = new CircleList<>(snapshotCount);
        ClassPathResource classPathResource = new ClassPathResource(snapshotPath);
        File file = classPathResource.getFile();
        File ssFile = new File(file, "snapshot.bin");
        if (!ssFile.exists()){
            ssFile.createNewFile();
        }
        writer = new BufferedOutputStream(new FileOutputStream(ssFile, true));
        ClassPathResource resource = new ClassPathResource(imageName);
        // TODO init from snapshot
        String imageID = imageName.substring(imageName.lastIndexOf('/') + 1, imageName.lastIndexOf('.'));
        map = new Canvas(imageID, ImageIO.read(resource.getInputStream()));
        snapshot(-1);
    }

//    @PreDestroy
    public void delete(){
        // TODO
    }

    /**
     * 向命令队列添加命令，线程不安全
     * @param cmd 要添加的命令
     * @throws BaseException
     */
    public void addCommand(User user, Command cmd) throws BaseException {
        if (commands.isFull() && commands.firstPoint() >= writePos){
            throw new BaseException("服务器繁忙，请稍后再试");
        }
        if (!user.getFunnel().watering(cmd.dataSize())){
            throw new BaseException("操作过于频繁，请稍后再试");
        }
        commands.append(cmd);
    }

    /**
     * 创建快照
     * @param snapshotID 快照ID
     * @throws IOException
     */
    public void snapshot(int snapshotID) throws IOException {
        map.setCmdID(snapshotID);
        byte[] bytes = map.toByteArray();
        Snapshot snapshot = new Snapshot(snapshotID, map.getId(), map.getWidth(), map.getHeight(), bytes);
        sss.append(snapshot);
        byte[] ssID = CommonUtil.intToByteArray(snapshotID);
        // 这条命令占用空间大小
        writer.write(CommonUtil.intToByteArray(bytes.length + 8));
        writer.write(ssID);
        writer.write(bytes);
        // TODO 考虑使用线程操作IO
        writer.flush();
    }

    public void syncUsers(int cmdID){
        for (User user: userServer.getAllUser().values()){
            if (!user.getWaitAck() && cmdID > user.getOffsetID() + 1) {
                List<Command> cmds = commands.getRange(user.getOffsetID() + 1, cmdID);
                user.setWaitAck(true);
                Channel channel = userServer.getAllChannel().find(user.getChannelId());
                BaseResp resp = BaseResp.success(cmds, "sync");
                channel.writeAndFlush(new TextWebSocketFrame(JSON.toJSONString(resp)));
            }
        }
    }

    /**
     * 1. 按顺序执行bitmap命令
     * 2. 定时执行快照
     */
    @Override
    public void run() {
        while (true){
            try {
                Command first = commands.get(writePos);
                if (first != null){
                    if (!first.isExecuted()) {
                        writedPixel += first.paint(map);
                        first.setId(writePos);
                        first.setExecuted(true);
                        writePos++;

                        if (writedPixel * 1.0 / (map.getHeight() * map.getWidth()) > ssPixels || writePos % ssCmds == 0) {
                            snapshot(writePos);
                        }
                    }
                } else {
                    sleep(1000);
                }
                if (writePos % 200 == 0 || (System.currentTimeMillis() - lastFlush >= 1500)){
                    syncUsers(writePos);
                    lastFlush = System.currentTimeMillis();
                }
            } catch (Exception e){
                logger.error(e.getMessage());
            }
        }
    }

    /**
     * TODO 同步向所有客户端发送snapshot
     */
    public void syncSendSnapshotToAll(){

    }

    /**
     * 从snapshot恢复
     * @param cmdID
     */
    public void revertSnapshot(int cmdID) throws IOException {
        for (Snapshot ss: sss){
            if (ss.getId() == cmdID){
                BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(ss.getData()));
                map = new Canvas(ss.getMapID(), bufferedImage);
                syncSendSnapshotToAll();
                commands.clear();
                return;
            }
        }
        // TODO 内存中没有找到，尝试从磁盘读取snapshot
    }

//    public void publishCommand(int currCmdID){
//        for (User user: userServer.getAllUser().values()){
//            BaseResp resp = BaseResp.success(this.commands.getRange(user.getCmdID(), currCmdID));
//            user.getChannel().writeAndFlush(JSON.toJSONString(resp));
//        }
//    }

    public int getMapWidth() {
        return map.getWidth();
    }

    public int getMapHeight() {
        return map.getHeight();
    }

    public int getCommandPoint(){
        return commands.getPoint();
    }

    public int getCommandSize(){
        return commands.getSize();
    }

    public int getWritePos() {
        return writePos;
    }

    public Snapshot lastSnapshot(){
        return sss.last();
    }

    public Canvas getMap() {
        return map;
    }

    public void setMap(Canvas map) {
        this.map = map;
    }
}
