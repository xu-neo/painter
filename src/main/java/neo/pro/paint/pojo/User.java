package neo.pro.paint.pojo;

import com.alibaba.fastjson.annotation.JSONField;
import io.netty.channel.ChannelId;

import java.util.Date;


public class User {
    public static final int ROLE_AUDIENCE = 0;
    public static final int ROLE_PAINTER = 1;
    public static final int ROLE_ANONYMOUS = 2;
    public static final int FUNNEL_CAPACITY = 1200;
    public static final int FUNNEL_COUNT_PERSEC = 200;

    private String id;

    private String name;

    private int roleId;

    @JSONField(serialize = false)
    private ChannelId channelId;

    private Date joinDate;

    @JSONField(serialize = false)
    private int offsetID;

    private Funnel funnel;

    @JSONField(serialize = false)
    private boolean waitAck = false;

    public User() {
    }

    public User(String name) {
        this.name = name;
        // 配置化
        funnel = new Funnel(FUNNEL_CAPACITY, FUNNEL_COUNT_PERSEC, 1);
    }

    public User(String name, int roleId) {
        this.name = name;
        this.roleId = roleId;
        funnel = new Funnel(FUNNEL_CAPACITY, FUNNEL_COUNT_PERSEC, 1);
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", roleId=" + roleId +
                ", joinDate=" + joinDate +
                '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public ChannelId getChannelId() {
        return channelId;
    }

    public void setChannelId(ChannelId channelId) {
        this.channelId = channelId;
    }

    public Date getJoinDate() {
        return joinDate;
    }

    public void setJoinDate(Date joinDate) {
        this.joinDate = joinDate;
    }

    public int getOffsetID() {
        return offsetID;
    }

    public void setOffsetID(int offsetID) {
        this.offsetID = offsetID;
    }

    public Funnel getFunnel() {
        return funnel;
    }

    public boolean getWaitAck() {
        return waitAck;
    }

    public void setWaitAck(boolean waitAck) {
        this.waitAck = waitAck;
    }
}
