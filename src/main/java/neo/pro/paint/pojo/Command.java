package neo.pro.paint.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import javax.xml.ws.FaultAction;
import java.awt.*;
import java.util.Date;

/**
 * 画布修改命令
 * @param <T>要修改的数据
 */
public abstract class Command <T> {
    private int id;
    private String rgb;
    private int color;
    private String mapID;
    @JSONField(serialize = false)
    private String userID;
    @JSONField(serialize = false)
    private boolean executed;
    private T data;
    @JSONField(serialize = false)
    private long createDate;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @JSONField(serialize = false)
    public int getColor() {
        return color;
    }

    @JSONField(name = "rgb", ordinal = 2)
    public void setColor(String rgb) {
        String color = rgb.replaceFirst("^#", "");
        this.color = Integer.parseInt(color, 16);
    }

    public String getRgb() {
        return rgb;
    }

    @JSONField(ordinal=1)
    public void setRgb(String rgb) {
        this.rgb = rgb;
    }

    public String getMapID() {
        return mapID;
    }

    public void setMapID(String mapID) {
        this.mapID = mapID;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public long getCreateDate() {
        return createDate;
    }

    public void setCreateDate(long createDate) {
        this.createDate = createDate;
    }

    abstract public int paint(Canvas map);

    abstract public int dataSize();

    public Command() {
    }

    public Command(String rgb, String mapID, String userID, T data) {
        this.rgb = rgb;
        this.mapID = mapID;
        this.userID = userID;
        this.executed = false;
        this.data = data;
        setColor(rgb);
        this.createDate = System.currentTimeMillis();
    }

}
