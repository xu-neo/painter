package neo.pro.paint.pojo;

import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;

import java.lang.reflect.Type;

public class RgbDeserializer implements ObjectDeserializer {
    @Override
    public Integer deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
        String color = ((String) parser.parse()).replaceFirst("^#", "");
        return Integer.parseInt(color, 16);
    }

    @Override
    public int getFastMatchToken() {
        return 0;
    }
}
