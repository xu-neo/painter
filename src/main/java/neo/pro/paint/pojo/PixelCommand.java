package neo.pro.paint.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import java.util.List;

/**
 * 使用像素点位置表示要修改的像素
 */
public class PixelCommand extends Command<List<Integer>>{
    @JSONField(serialize = false)
    private int mapWidth;

    public PixelCommand(String rgb, List<Integer> pixels, String mapId, String userId, int mapWidth) {
        super(rgb, mapId, userId, pixels);
        this.mapWidth = mapWidth;
    }

    @Override
    public int paint(Canvas map) {
        for (Integer index: getData()){
            map.setRGB(index % mapWidth, index / mapWidth, getColor());
        }
        return getData().size();
    }

    @Override
    public int dataSize() {
        return getData().size();
    }

}
