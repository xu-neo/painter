package neo.pro.paint.pojo;

import neo.pro.paint.exception.BaseException;
import neo.pro.paint.utils.Const;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BaseResp {
    private static final Logger logger= LoggerFactory.getLogger(BaseResp.class);

    private int code;
    private String msg;
    private Object data;
    private String action;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public BaseResp(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public BaseResp(int code, String msg, Object data, String action) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.action = action;
    }

    public static BaseResp success(Object data){
        return new BaseResp(Const.SUCC_CODE, "", data);
    }

    public static BaseResp success(Object data, String action){
        return new BaseResp(Const.SUCC_CODE, "", data, action);
    }

    public static BaseResp error(BaseException e){
        logger.error(e.getMessage());
        return new BaseResp(e.getCode(), e.getMessage(), null);
    }

    public static BaseResp error(Throwable e){
        logger.error(e.getMessage());
        return new BaseResp(Const.ERROR_CODE, e.getMessage(), null);
    }

}
