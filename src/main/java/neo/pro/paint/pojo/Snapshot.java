package neo.pro.paint.pojo;


import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.parser.DefaultJSONParser;
import com.alibaba.fastjson.parser.JSONToken;
import com.alibaba.fastjson.parser.deserializer.ObjectDeserializer;
import com.alibaba.fastjson.serializer.JSONSerializer;
import com.alibaba.fastjson.serializer.ObjectSerializer;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.Base64;

public class Snapshot implements Serializable {
    private int id;
    private String mapID;
    private int width;
    private int height;
    @JSONField(serializeUsing = SnapshotDataSerializer.class, deserializeUsing = SnapshotDataDeserializer.class)
    private byte[] data;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMapID() {
        return mapID;
    }

    public void setMapID(String mapID) {
        this.mapID = mapID;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }

    public Snapshot(int id, String mapID, int width, int height, byte[] data) {
        this.id = id;
        this.mapID = mapID;
        this.width = width;
        this.height = height;
        this.data = data;
    }

    static public class SnapshotDataSerializer implements ObjectSerializer {

        @Override
        public void write(JSONSerializer serializer, Object object, Object fieldName, Type fieldType, int features) throws IOException {
            serializer.write(Base64.getEncoder().encodeToString((byte[]) object));
        }
    }

    static public class SnapshotDataDeserializer implements ObjectDeserializer{

        @Override
        public <T> T deserialze(DefaultJSONParser parser, Type type, Object fieldName) {
            String s = parser.parseObject(String.class);
            byte[] decode = Base64.getDecoder().decode(s);
            return (T) decode;
        }

        @Override
        public int getFastMatchToken() {
            return JSONToken.UNDEFINED;
        }
    }
}
