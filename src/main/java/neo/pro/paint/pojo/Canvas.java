package neo.pro.paint.pojo;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.Hashtable;

public class Canvas extends BufferedImage implements Cloneable{
    private String id;
    private int cmdID;

    public Canvas(String id, BufferedImage bi) {
        super(bi.getColorModel(), bi.getRaster(), bi.isAlphaPremultiplied(), null);
        this.id = id;
    }

    public Canvas(ColorModel cm, WritableRaster raster, boolean isRasterPremultiplied, Hashtable<?, ?> properties) {
        super(cm, raster, isRasterPremultiplied, properties);
    }

    @Override
    public Canvas clone() {
        Canvas canvas = new Canvas(getColorModel(),(WritableRaster) getData(), isAlphaPremultiplied(), null);
        canvas.setId(id);
        canvas.setCmdID(cmdID);

        return canvas;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getCmdID() {
        return cmdID;
    }

    public void setCmdID(int cmdID) {
        this.cmdID = cmdID;
    }

    public byte[] toByteArray() throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(this, "PNG", baos);
        return baos.toByteArray();
    }

    public String base64Encoding() throws IOException {
        return Base64.getEncoder().encodeToString(toByteArray());
    }
}
