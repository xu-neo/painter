package neo.pro.paint.pojo;

import com.alibaba.fastjson.annotation.JSONField;

import java.awt.*;
import java.util.List;

public class RectCommand extends Command<List<Integer>>{
    private int width;
    @JSONField(serialize = false)
    private int mapWidth;

    public int getMapWidth() {
        return mapWidth;
    }

    public void setMapWidth(int mapWidth) {
        this.mapWidth = mapWidth;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public RectCommand(int width, String rgb, List<Integer> data, String mapID, String userID) {
        super(rgb, mapID, userID, data);
        this.width = width;
    }

    @Override
    public int paint(Canvas map) {
        int count = 0;
        Graphics2D graphics = map.createGraphics();
        graphics.setColor(new Color(getColor()));
        for (Integer index: getData()){
            graphics.fillRect(index % mapWidth, index / mapWidth, width, width);
            count += width * width;
        }
        graphics.dispose();
        return count;
    }

    @Override
    public int dataSize() {
        int count = 0;
        for (Integer i: getData()){
            count += width * width;
        }
        return count;
    }
}
