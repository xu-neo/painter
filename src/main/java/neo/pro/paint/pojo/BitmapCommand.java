package neo.pro.paint.pojo;

import java.util.Iterator;

/**
 * 使用位图表示要修改的数据
 */
public class BitmapCommand extends Command<byte[]> implements Iterable<Integer[]>{
    private int mapX;
    private int mapY;
    private int width;
    private int size;

    public int getMapX() {
        return mapX;
    }

    public void setMapX(int mapX) {
        this.mapX = mapX;
    }

    public int getMapY() {
        return mapY;
    }

    public void setMapY(int mapY) {
        this.mapY = mapY;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public BitmapCommand(int mapX, int mapY, int width, String rgb, byte[] bitmap, String mapID, String userID, int size) {
        super(rgb, mapID, userID, bitmap);
        this.mapX = mapX;
        this.mapY = mapY;
        this.width = width;
        this.size = size;
    }

    @Override
    public int paint(Canvas map) {
        int count = 0;
        for (Integer[] pixel : this) {
            map.setRGB(pixel[0], pixel[1], getColor());
            count ++;
        }
        return count;
    }

    @Override
    public int dataSize() {
        return size;
    }

    @Override
    public Iterator<Integer[]> iterator() {
        return new PixelIterator();
    }

    /**
     * 遍历位图，返回像素位置
     */
    private class PixelIterator implements Iterator<Integer[]> {
        private int i1 = 0;
        private int i2 = 0;

        @Override
        public boolean hasNext() {
            return i1 < getData().length;
        }

        @Override
        public Integer[] next() {
            byte[] data = getData();
            for (int i = i1; i < data.length; i ++) {
                byte b = (byte) (data[i] << i2);
                for (int j = i2; j < 8; j++) {
                    if (b < 0) {
                        int index = i1 * 8 + j;
                        int _mapX = index % width + mapX;
                        int _mapY = index / width + mapY;
                        i1 = i;
                        i2 = j + 1;
                        return new Integer[]{_mapX, _mapY};
                    }
                    b = (byte) (b << 1);
                }
            }
            return null;
        }
    }
}
