package neo.pro.paint.pojo;

public class CommandInfo{
    private int point;
    private int size;
    private int writePos;

    public CommandInfo(int point, int size, int writePos) {
        this.point = point;
        this.size = size;
        this.writePos = writePos;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getWritePos() {
        return writePos;
    }

    public void setWritePos(int writePos) {
        this.writePos = writePos;
    }
}