package neo.pro.paint.exception;

import neo.pro.paint.pojo.BaseResp;
import neo.pro.paint.utils.Const;

public class BaseException extends Exception{
    private int code = Const.ERROR_CODE;

    public BaseException(String message, int code) {
        super(message);
        this.code = code;
    }

    public BaseException(String message){
        super(message);
    }

    public BaseException(){
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }
}
