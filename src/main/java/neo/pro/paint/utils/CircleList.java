package neo.pro.paint.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * 循环存储列表，线程不安全
 */
public class CircleList<E> implements Iterable<E>{

    private int size;

    // 下一个添加位置
    private int point = 0;

    private E[] data;

    public CircleList(int size){
        this.size = size;
        this.data = (E[]) new Object[size];
    }

    /**
     * 返回列表添加位置
     */
    public int getPoint() {
        return point;
    }

    /**
     * 获取一条命令
     * @param index 命令id
     */
    public E get(int index) {
        if (index >= point || index < firstPoint()){
            return null;
        }
        return (E) data[index % size];
    }

    /**
     * 根据范围获取命令
     * @param start 起始命令的id，包含
     * @param end 结束命令的id，不包含
     * @return 命令数组
     */
    public List<E> getRange(int start, int end) {
        if (end <= start){
            return new ArrayList<>();
        }
        int startIndex = start % size;
        int endIndex = end % size;
        if (end - start >= size){
            return Arrays.asList(data);
        } else if (startIndex < endIndex) {
            List<E> res = new ArrayList<>();
            for (int i = startIndex; i < endIndex; i ++){
                res.add(data[i]);
            }
            return res;
        } else {
            List<E> res = new ArrayList<>();
            int index = 0;
            for (int i = startIndex; i < size; i++){
                res.add(data[i]);
                index ++;
            }
            for (int i = 0; i < endIndex; i ++){
                res.add(data[i]);
                index ++;
            }
            return res;
        }
    }

    /**
     * 添加一条命令
     * @param e 要添加的命令
     */
    public int append(E e) {
        this.data[point % size] = e;
        point ++;
        return point - 1;
    }

    public int getSize() {
        return size;
    }

    public boolean isFull(){
        return size - point <= 0;
    }

    public int firstPoint(){
        return Math.max(0, point - size);
    }

    public E first(){
        return get(firstPoint());
    }

    public E last(){
        return get(point - 1);
    }

    public boolean isEmpty(){
        return point == 0;
    }

    public void clear(){
        data = (E[]) new Object[size];
        point = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new Itr();
    }

    private class Itr implements Iterator<E> {
        private int cursor;
        private int end;

        public Itr(){
            cursor = firstPoint();
            end = point;
        }

        @Override
        public boolean hasNext() {
            return point != 0 && cursor < end;
        }

        @Override
        public E next() {
            E res = get(cursor);
            cursor ++;
            return res;
        }
    }
}
