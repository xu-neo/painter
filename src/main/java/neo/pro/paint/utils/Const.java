package neo.pro.paint.utils;

import org.springframework.stereotype.Component;

@Component
public class Const {
    public static final String PAINTER_COUNT_KEY = "painter_count";
    public static final String AUDIENCE_COUNT_KEY = "audience_count";
    public static final String USER_KEY_PREFIX = "USER_";
    public static final int SUCC_CODE = 0;
    public static final int ERROR_CODE = 200;
    public static final int FAIL_CODE = 100;
}
