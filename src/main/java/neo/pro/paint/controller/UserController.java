package neo.pro.paint.controller;

import io.netty.channel.ChannelHandlerContext;
import neo.pro.paint.annotation.WsMapping;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.BaseResp;
import neo.pro.paint.pojo.Snapshot;
import neo.pro.paint.pojo.User;
import neo.pro.paint.server.UserServer;
import neo.pro.paint.server.MapServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@RestController
public class UserController {
    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Value("${paint.painter.count}")
    private int painterCount;

    @Autowired
    private UserServer userServer;

    @Autowired
    private MapServer mapServer;

    /**
     * 用户请求参与画画
     * @param ctx ChannelHandlerContext类
     * @param name 用户名
     * @return 用户身份
     */
    @WsMapping
    public BaseResp join(ChannelHandlerContext ctx, String name) throws BaseException {
        if (name == null || name.equals("")){
            name = ctx.channel().id().asShortText();
        }
        User user = new User(name);
        userServer.validateUser(user);
        user.setChannelId(ctx.channel().id());
        user.setJoinDate(new Date());
        user.setId(ctx.channel().id().asShortText());
         if (userServer.painterSize() >= painterCount){
            user.setRoleId(User.ROLE_AUDIENCE);
            logger.info("{} join to audience", user);
            userServer.addAudience(user, ctx.channel());
        } else {
            user.setRoleId(User.ROLE_PAINTER);
            logger.info("{} join to paint", user);
            userServer.addPainter(user, ctx.channel());
        }
        Snapshot snapshot = mapServer.lastSnapshot();
         user.setOffsetID(snapshot.getId());
        Map<String, Object> res = new HashMap<>();
        res.put("user", user);
        res.put("snapshot", snapshot);
        return BaseResp.success(res);
    }
}
