package neo.pro.paint.controller;

import io.netty.channel.ChannelHandlerContext;
import neo.pro.paint.annotation.WsMapping;
import neo.pro.paint.pojo.BaseResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;

@Controller
public class HelloController {
    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @WsMapping
    public BaseResp hello(ChannelHandlerContext ctx, String s1, int i){
        return BaseResp.success("hello " + ctx.channel().id().asShortText());
    }
}
