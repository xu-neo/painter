package neo.pro.paint.controller;

import io.netty.channel.ChannelHandlerContext;
import neo.pro.paint.annotation.WsMapping;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.RectCommand;
import neo.pro.paint.pojo.User;
import neo.pro.paint.server.MapServer;
import neo.pro.paint.pojo.BaseResp;
import neo.pro.paint.server.UserServer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RestController
@RequestMapping("/initMap")
public class MapController {
    private final Logger logger= LoggerFactory.getLogger(this.getClass());

    @Autowired
    private MapServer mapServer;

    @Autowired
    private UserServer userServer;

    @GetMapping("")
    public BaseResp initMap(@RequestParam String imageName) throws IOException {
        ClassPathResource resource = new ClassPathResource("static/images/" + imageName);
        BufferedImage bufImage = ImageIO.read(resource.getInputStream());
        return BaseResp.success("mapServer.initMap(bufImage)");
    }

    private User validateRectCmd(RectCommand cmd, ChannelHandlerContext ctx) throws BaseException{
        if (cmd.getWidth() < 0 || cmd.getWidth() > 10){
            throw new BaseException("线宽不合规");
        }
        if (cmd.getMapID() != null && !cmd.getMapID().equals(mapServer.getMap().getId())){
            throw new BaseException("valid mapID");
        }
        String s = ctx.channel().id().asShortText();
        User user = userServer.getUserByID(s);
        if (user == null){
            throw new BaseException("not find user[" + s + "]");
        }
        return user;
    }

    /**
     * 向命令队列添加一条命令
     * @throws BaseException
     */
    @WsMapping
    public BaseResp write(ChannelHandlerContext ctx, RectCommand stroke) throws BaseException {
        User user = validateRectCmd(stroke, ctx);
        stroke.setMapWidth(mapServer.getMapWidth());
        mapServer.addCommand(user, stroke);
        return BaseResp.success(user.getFunnel());
    }

    @WsMapping
    public void ack(ChannelHandlerContext ctx, int offsetID){
        User user = userServer.getAllUser().get(ctx.channel().id().asShortText());
        user.setWaitAck(false);
        user.setOffsetID(offsetID);
    }
}
