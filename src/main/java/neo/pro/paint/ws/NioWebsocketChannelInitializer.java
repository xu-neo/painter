package neo.pro.paint.ws;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class NioWebsocketChannelInitializer extends ChannelInitializer<SocketChannel> {
    @Value("${server.netty.host}")
    private String host;

    @Value("${server.netty.port}")
    private String port;

    @Value("${server.websocket.url}")
    private String wsUrl;

    @Autowired
    private WsHandler wsHandler;

    @Override
    protected void initChannel(SocketChannel ch) {
        ch.pipeline().addLast(new HttpServerCodec());
        ch.pipeline().addLast(new HttpObjectAggregator(65536));
        ch.pipeline().addLast(new ChunkedWriteHandler());//用于大数据的分区传输
        // TODO zlib
//        ch.pipeline().addLast(new WebSocketServerCompressionHandler());
        ch.pipeline().addLast(new WebSocketServerProtocolHandler(wsUrl));
        ch.pipeline().addLast(new HttpHandler(wsUrl));
        ch.pipeline().addLast(wsHandler);
    }
}