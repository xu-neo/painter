package neo.pro.paint.ws;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ChannelSupervise {
    private static ChannelGroup PainterGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private static ChannelGroup AudienceGroup = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);
    private static Map<String, Channel> allChannel = new HashMap<>();

    private static ConcurrentMap<String, ChannelId> ChannelMap = new ConcurrentHashMap<>();

    public static void addPainterChannel(Channel channel){
        PainterGroup.add(channel);
        allChannel.put(channel.id().asShortText(), channel);
    }

    public static void removePainterChannel(Channel channel){
        PainterGroup.remove(channel);
    }

    public static void addAudienceChannel(Channel channel){
        AudienceGroup.add(channel);
    }

    public static void removeAudienceChannel(Channel channel){
        AudienceGroup.remove(channel);
    }

    public static void send2Painter(TextWebSocketFrame tws){
        PainterGroup.writeAndFlush(tws);
    }

    public static void send2Audience(TextWebSocketFrame tws){
        AudienceGroup.writeAndFlush(tws);
    }

    public static int painterSize(){
        return PainterGroup.size();
    }

    public static int audienceSize(){
        return AudienceGroup.size();
    }
}
