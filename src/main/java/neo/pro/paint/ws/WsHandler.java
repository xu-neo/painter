package neo.pro.paint.ws;

import com.alibaba.fastjson.JSON;
import io.netty.channel.ChannelHandler;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.websocketx.*;
import neo.pro.paint.config.WsDispatchConfig;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.BaseResp;
import neo.pro.paint.pojo.User;
import neo.pro.paint.server.MapServer;
import neo.pro.paint.server.UserServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ChannelHandler.Sharable
@Component
public class WsHandler extends SimpleChannelInboundHandler<TextWebSocketFrame> {

    private final Logger logger= LoggerFactory.getLogger(this.getClass());
    @Autowired
    private WsDispatchConfig wsDispatchConfig;
    @Autowired
    private MapServer mapServer;
    @Autowired
    private UserServer userServer;

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, TextWebSocketFrame msg) {
        handlerWebSocketFrame(ctx, msg);
    }

    /**
     * 当系统负载较高时，拒绝新的连接
     */
    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        logger.debug("客户端加入连接："+ctx.channel());
        int unfinish = mapServer.getCommandPoint() - mapServer.getWritePos();
        double executiveRate = unfinish * 1.0 / mapServer.getCommandSize();
        if (executiveRate >= 0.94 || mapServer.getCommandSize() - unfinish < 10){
            ctx.close();
        }
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        //断开连接
        User user = userServer.getAllUser().remove(ctx.channel().id().asShortText());
        logger.debug("客户端断开连接："+ user.getId());
//        ChannelSupervise.removeChannel(ctx.channel());
    }

    @Override
    public void channelReadComplete(ChannelHandlerContext ctx) {
        ctx.flush();
    }

    private void handlerWebSocketFrame(ChannelHandlerContext ctx, TextWebSocketFrame frame){
        // 返回应答消息
        try {
            String request = frame.text();
            BaseResp result = wsDispatchConfig.dispatch(ctx, request);
            if (result != null){
                resp(result, ctx);
            }
        } catch (Exception e){
            logger.error("catch an error", e);
            BaseResp baseResp = BaseResp.error(e);
            resp(baseResp, ctx);
        }
        // 群发
//        ChannelSupervise.send2All(tws);
        // 返回【谁发的发给谁】
        // ctx.channel().writeAndFlush(tws);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable e){
        logger.error(e.getMessage());
        ctx.close();
    }

    private void resp(BaseResp result, ChannelHandlerContext ctx){
        String s = JSON.toJSONString(result);
        // TODO new BinaryWebSocketFrame()
        ctx.writeAndFlush(new TextWebSocketFrame(s));
    }
}