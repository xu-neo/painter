package neo.pro.paint.config;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import neo.pro.paint.annotation.WsMapping;
import neo.pro.paint.exception.BaseException;
import neo.pro.paint.pojo.BaseResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.filter.AnnotationTypeFilter;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import javax.annotation.PostConstruct;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.*;

@Component
public class WsDispatchConfig implements ApplicationContextAware {
    private final Logger logger= LoggerFactory.getLogger(this.getClass());
    private Map<String, Method> dispatchMap = new HashMap<>();;
    private  ApplicationContext applicationContext;

    @PostConstruct
    public void buildDispatchMap() {
        ClassPathScanningCandidateComponentProvider scanner = new ClassPathScanningCandidateComponentProvider(false);
        scanner.addIncludeFilter(new AnnotationTypeFilter(Controller.class));
        scanner.findCandidateComponents("neo.pro.paint.controller").forEach(beanDefinition -> {
            try {
                Method[] methods = Class.forName(beanDefinition.getBeanClassName()).getMethods();
                for (Method method: methods){
                    WsMapping annotation = method.getAnnotation(WsMapping.class);
                    if (annotation != null){
                        String commandName = annotation.value();
                        if (commandName.equals("")){
                            commandName = method.getName();
                        }
                        dispatchMap.put(commandName, method);
                    }
                }

            } catch (Exception e){
                logger.error(e.getMessage());
            }
        });
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public BaseResp dispatch(ChannelHandlerContext ctx, String command) throws Exception{
        JSONObject cmdObj = JSON.parseObject(command);
        String action = (String)cmdObj.remove("action");
        try {
            if (action == null){
                throw new BaseException("missing field: action");
            }
            Method method = dispatchMap.get(action);
            if (method == null){
                throw new BaseException("command[" + action + "] is invalid");
            }
            Object bean = applicationContext.getBean(method.getDeclaringClass());
            Parameter[] parameters = method.getParameters();
            Object[] args = new Object[parameters.length];
            for (int i = 0; i < parameters.length; i ++) {
                if (parameters[i].getType().isInstance(ctx)) {
                    args[0] = ctx;
                } else if (cmdObj.containsKey(parameters[i].getName())){
                    args[i] = cmdObj.getObject(parameters[i].getName(), parameters[i].getType());
                } else {
                    throw new BaseException("missing field: " + parameters[i].getName());
                }
            }
            BaseResp resp = (BaseResp) method.invoke(bean, args);
            if (resp != null){
                resp.setAction(action);
            }
            return resp;
        } catch (InvocationTargetException e){
            Throwable targetException = e.getTargetException();
            BaseResp error = BaseResp.error(targetException);
            logger.error(ctx.channel().id().asShortText() + "客户端异常：" + targetException.getMessage());
            error.setAction(action);
            return error;
        }
    }

}
