package neo.pro.paint;


import ove.crypto.digest.Blake2b;

import java.util.Base64;


public class CommonTest {
    static final private String testStr = "ssh://root@192.168.0.231:22/opt/baisanqi/callcenter_new/venv/bin/python1111";

    public static String byteToArray(byte[]data){
        StringBuilder result= new StringBuilder();
        for (byte datum : data) {
            result.append(Integer.toHexString((datum & 0xFF) | 0x100), 1, 3);
        }
        return result.toString();
    }

    public static void main(String[] args) {
        Blake2b.Param param = new Blake2b.Param()
                .setDigestLength(32)
                .setKey("bb56a79b3842accc".getBytes())
                .setSalt("1641279333864".getBytes());
        Blake2b.Digest digest = Blake2b.Digest.newInstance(param);
        digest.update(testStr.getBytes());
        byte[] res = digest.digest();
        System.out.println(Base64.getEncoder().encodeToString(res));
    }
}
